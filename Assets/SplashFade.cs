﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashFade : MonoBehaviour {

	public Image SplashImage;
	public string LoadLevel;


	IEnumerator Start()
	{
		SplashImage.canvasRenderer.SetAlpha (0.0f);

		fadeIn ();
		yield return new WaitForSeconds (2.5f);

		fadeout ();

		yield return new WaitForSeconds (2.5f);

		fadeIn ();

		yield return new WaitForSeconds (2.5f);

		fadeout ();
		yield return new WaitForSeconds (2.5f);

		SceneManager.LoadScene (LoadLevel);


	}


	void fadeIn()
	{
		SplashImage.CrossFadeAlpha (1f, 1.5f, false);


	}


	void fadeout()
	{
		SplashImage.CrossFadeAlpha (0f, 2.5f, false);

	}

}
